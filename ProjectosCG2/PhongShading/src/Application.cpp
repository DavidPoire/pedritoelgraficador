
#include "Application.h"
#include <iostream>
#include <vector>
#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include "ShaderFuncs.h"

Application::Application():oPlane() {
	eye = glm::vec3(0.0f, 50.0f, 100.0f);
	target = glm::vec3(0.0f, 0.0f, 0.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);
	time = 0;
	myLightPosition = glm::vec3(0.0f, 50.0f, -100.0f);
	lA = glm::vec3(0.2, 0.2, 0.2);
	lD = glm::vec3(0.4, 0.4, 0.4);
	lS = glm::vec3(1.0);
	mA = glm::vec3(1.0);
	mD = glm::vec3(1.0);
	mS = glm::vec3(1.0);



}

Application::~Application() {}

void Application::update(){
	time += .5f;
}

void Application::setup(){
	oPlane.createPlane(10);


	std::string strVertexShader = loadTextFile("Shaders/PhongShading.v");
	std::string strFragmentShader = loadTextFile("Shaders/PhongShading.f");
	InitializeProgram(oPlane.shader, strVertexShader, strFragmentShader);

	mTransformID = glGetUniformLocation(oPlane.shader, "mTransform");
	cameraID = glGetUniformLocation(oPlane.shader, "camera");
	perspectiveID = glGetUniformLocation(oPlane.shader, "perspective");

	fTimeID = glGetUniformLocation(oPlane.shader, "fTime");
	eyeID = glGetUniformLocation(oPlane.shader, "vEye");
	myLightPositionID = glGetUniformLocation(oPlane.shader, "myLightPosition");

	laID = glGetUniformLocation(oPlane.shader, "lA");
	lsID = glGetUniformLocation(oPlane.shader, "lS");
	ldID = glGetUniformLocation(oPlane.shader, "lD");
	maID = glGetUniformLocation(oPlane.shader, "mA");
	mdID = glGetUniformLocation(oPlane.shader, "mD");
	msID = glGetUniformLocation(oPlane.shader, "mS");

	glGenVertexArrays(1, &oPlane.vao);
	glBindVertexArray(oPlane.vao);
	glGenBuffers(1, &oPlane.vbo);
	glBindBuffer(GL_ARRAY_BUFFER, oPlane.vbo);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPositions), vertexPositions, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, oPlane.getVertexSizeInBytes() + oPlane.getTextureCoordsSizeInBytes(), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, oPlane.getVertexSizeInBytes(), oPlane.plane);
	glBufferSubData(GL_ARRAY_BUFFER, oPlane.getVertexSizeInBytes(), oPlane.getTextureCoordsSizeInBytes(), oPlane.textureCoords);
	oPlane.cleanMemory();

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	//glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)(oPlane.getVertexSizeInBytes-

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	//glEnable(GL_DEPTH_TEST);
}

void Application::display(){
	//Borramos el buffer de color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Seleccionamos los shaders a usar
	glUseProgram(oPlane.shader);	
	
	//activamos el vertex array object y dibujamos
	glBindVertexArray(oPlane.vao);
	lookAt = glm::lookAt(eye, target, up);
	perspective = glm::perspective(45.0f, 640.0f / 480.0f, 0.1f, 200.0f);
	transform = glm::mat4(1.0f);

	//transformaciones del modelo
	glUniformMatrix4fv(mTransformID, 1, false, glm::value_ptr(transform));

	//transformacion de camara
	glUniformMatrix4fv(cameraID, 1, false, glm::value_ptr(lookAt));

	//transformacion de proyeccion
	glUniformMatrix4fv(perspectiveID, 1, false, glm::value_ptr(perspective));


	//parametro de fase para shaders
	glUniform1f(fTimeID, glm::radians(time));
	glUniform3fv(eyeID, 1, glm::value_ptr(eye));
	glm::vec3 newLight = myLightPosition;
	newLight.z = myLightPosition.z * cos(glm::radians(time));
	glUniform3fv(myLightPositionID, 1, glm::value_ptr(newLight));



	glUniform3fv(laID, 1, glm::value_ptr(lA));
	glUniform3fv(ldID, 1, glm::value_ptr(lD));
	glUniform3fv(lsID, 1, glm::value_ptr(lS));
	glUniform3fv(maID, 1, glm::value_ptr(mA));
	glUniform3fv(msID, 1, glm::value_ptr(mS));
	glUniform3fv(mdID, 1, glm::value_ptr(mD));


	//glUniform3fv(oPlane.uEye, 1, glm::value_ptr(eye));
	glDrawArrays(GL_TRIANGLES, 0, oPlane.getNumVertex());
}

void Application::reshape(int w, int h){
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void Application::keyboard(int key, int scancode, int action, int mods){
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		exit(0);
	}
	else if (key == GLFW_KEY_A && action == GLFW_PRESS)
	{
		lA += glm::vec3(0.1);
	}
	else if (key == GLFW_KEY_S && action == GLFW_PRESS)
	{
		lS += glm::vec3(0.1);
	}
	else if (key == GLFW_KEY_D && action == GLFW_PRESS)
	{
		lD += glm::vec3(0.1);
	}
	else if (key == GLFW_KEY_M && action == GLFW_PRESS)
	{
		mA += glm::vec3(0.1);
	}
	else if (key == GLFW_KEY_N && action == GLFW_PRESS)
	{
		mS += glm::vec3(0.1);
	}
	else if (key == GLFW_KEY_B && action == GLFW_PRESS)
	{
		mD += glm::vec3(0.1);
	}
	else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
	{
		lA = glm::vec3(0.2, 0.2, 0.2);
		lS = glm::vec3(1.0, 1.0, 1.0);
		lD = glm::vec3(0.4, 0.4, 0.4);
		mA = glm::vec3(1.0, 0.5, 0.0);
		mS = glm::vec3(1.0, 1.0, 1.0);
		mD = glm::vec3(1.0, 0.5, 0.0);
	}
	if (action == GLFW_RELEASE) {
		return;
	}
}
