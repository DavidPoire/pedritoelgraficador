	#version 430 core

	layout (location = 0) in vec4 vPosition;
	uniform mat4 transform;

	void main()
	{
		
		color = vColor;

		gl_Position =  vPosition;
	}
	