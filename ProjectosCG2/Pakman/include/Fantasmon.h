#pragma once
#include <iostream>

#include "Player.h"
class Fantasmon
{
public:
	Fantasmon();
	~Fantasmon();

	int x, y;

	void setup(int _x, int _y);


	bool moveUp(std::vector<std::vector<int> >&);
	bool moveDown(std::vector<std::vector<int> >&);
	bool moveLeft(std::vector<std::vector<int> >&);
	bool moveRight(std::vector<std::vector<int> >&);

	bool CheckPacman(std::vector<std::vector<int> >);


	void artificialIneligence(std::vector<std::vector<int> >&, Player);


};

