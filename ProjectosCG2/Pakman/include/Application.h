#pragma once

#include <iostream>
#include "Object3D.h"
#include "glm/glm.hpp"
#include "GLFW/glfw3.h"
#include "Player.h"
#include "Fantasmon.h"



class Application {
public:
	Application();
	~Application();

	void setup();
	void keyboard(int , int , int , int );
	void update();
	void displayGame();
	void displayEditor();
	void display();
	void reshape(int w, int h);

	void goUp();
	void goDown();
	void goLeft();
	void goRight();


	void moveUp(Fantasmon);
	void moveDown(Fantasmon);
	void moveLeft(Fantasmon);
	void moveRight(Fantasmon);

	void artificialInteligence(Fantasmon fan);

	void ChangeNumber(int _x, int _y, int newNum);


	GLFWwindow* window;



private:	
	GLuint angleID, transformID;
	Object3D triangle;

	glm::mat4 camera;
	glm::mat4 transform, transform2;
	glm::vec3 eye, target, angles;

	Player PacMan;

	Fantasmon uno;
	Fantasmon dos;
	Fantasmon tres;
	Fantasmon cuatro;

	char yi = 0;

	glm::vec4 ghostColor[4] =
	{
		glm::vec4(1.0f,0.0f,1.0f,1.0f),
		glm::vec4(0.0f,1.0f,1.0f,1.0f),
		glm::vec4(0.5f,0.5f,1.0f,1.0f),
		glm::vec4(1.0f,0.5f,0.5f,1.0)
	};

	bool Edit, write;

	std::vector< std::vector<int> > map
	{
		{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 },
		{ 1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,1 },
		{ 1,0,1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,1,0,1 },
		{ 1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,1 },
		{ 1,0,1,0,1,1,0,1,1,0,1,1,0,1,1,1,0,1,0,1 },
		{ 1,0,1,0,1,1,0,1,1,0,1,1,0,1,1,1,0,1,0,1 },
		{ 1,0,1,0,1,1,0,0,1,0,1,0,0,1,1,1,0,1,0,1 },
		{ 1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,1 },
		{ 1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1 },
		{ 1,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,1 },
		{ 1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1 },
		{ 1,0,0,0,0,0,0,0,1,0,1,0,0,1,1,1,1,1,0,1 },
		{ 1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,1 },
		{ 1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,0,1 },
		{ 1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,1 },
		{ 1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,0,1 },
		{ 1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,1 },
		{ 1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,0,1 },
		{ 1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,1 },
		{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 },

	};
	enum pacmanMap
	{
		coins = 0,
		wall,
		pacman,
		seen,
		ghost
	};

	

};

