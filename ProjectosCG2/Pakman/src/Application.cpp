
#include "Application.h"
#include <iostream>
#include <vector>
#include "glm/gtc/matrix_transform.hpp"
#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include "glm/gtc/type_ptr.hpp"
#include "ShaderFuncs.h"


#define UP 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3



std::vector<GLfloat> vertexPositions = {
	 1.0f,   1.0f, 1.0f, 1.0f,//a
	-1.0f,  -1.0f, 1.0f, 1.0f,//c
	 1.0f,  -1.0f, 1.0f, 1.0f,//b
	 1.0f,   1.0f, 1.0f, 1.0f,//a
	-1.0f,   1.0f, 1.0f, 1.0f,//d
	-1.0f,  -1.0f, 1.0f, 1.0f,//c
	//Cara 1
	// -------------------- 
	//Cara 2

	  1.0f, 1.0f, -1.0f, 1.0f,//e
	 -1.0f, 1.0f,  1.0f, 1.0f,//d
	  1.0f, 1.0f,  1.0f, 1.0f,//a
	  1.0f, 1.0f, -1.0f, 1.0f,//e
	 -1.0f, 1.0f, -1.0f, 1.0f,//f
	 -1.0f, 1.0f,  1.0f, 1.0f,//d
	//Cara 2
	// -------------------- 
	//Cara 3

	-1.0f, 1.0f,  1.0f, 1.0f,//d
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	-1.0f,-1.0f,  1.0f, 1.0f,//c
	-1.0f, 1.0f,  1.0f, 1.0f,//d
	-1.0f, 1.0f, -1.0f, 1.0f,//f
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	//Cara 3
	// -------------------- 
	//Cara 4

	1.0f, 1.0f, -1.0f, 1.0f,//e
	1.0f,-1.0f,  1.0f, 1.0f,//b
	1.0f,-1.0f, -1.0f, 1.0f,//h
	1.0f, 1.0f, -1.0f, 1.0f,//e
	1.0f, 1.0f,  1.0f, 1.0f,//a
	1.0f,-1.0f,  1.0f, 1.0f,//b
	//Cara 4
	// -------------------- 
	//Cara 5

	 1.0f,-1.0f,  1.0f, 1.0f,//b
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	 1.0f,-1.0f, -1.0f, 1.0f,//h
	 1.0f,-1.0f,  1.0f, 1.0f,//b
	-1.0f,-1.0f,  1.0f, 1.0f,//c
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	//Cara 5
	// -------------------- 
	//Cara 6

	 1.0f,-1.0f, -1.0f, 1.0f,//h
	-1.0f, 1.0f, -1.0f, 1.0f,//f
	 1.0f, 1.0f, -1.0f, 1.0f,//e
	 1.0f,-1.0f, -1.0f, 1.0f,//h
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	-1.0f, 1.0f, -1.0f, 1.0f,//f


};



Application::Application() : eye(10.0f,95.0f,5.0f),
							 target(18.0f,0.0f,18.0f),
							 transform(glm::mat4(1.0f)),
							 angles(1.0f,0.0f,0.0f)
{}

Application::~Application() 
{}


void Application::update()
{
	angles.x +=0.05f;
	angles.y +=0.05f;
		camera = glm::lookAt(eye,target,glm::vec3(0.0f,1.0f,0.0f));
	

		if (PacMan.x < 10 && PacMan.y < 10)
		{
			uno.artificialIneligence(map, PacMan);
		}
		else if (PacMan.x > 10 && PacMan.y > 10)
		{
			dos.artificialIneligence(map, PacMan);
		}
		else if (PacMan.x < 10 && PacMan.y > 10)
		{
			tres.artificialIneligence(map, PacMan);
		}
		else if (PacMan.x > 10 && PacMan.y < 10)
		{
			cuatro.artificialIneligence(map, PacMan);
		}


		if (uno.CheckPacman(map) == true || dos.CheckPacman(map) == true || 
			tres.CheckPacman(map) == true || cuatro.CheckPacman(map) == true)
			exit(0);

}

void Application::displayGame()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	eye = glm::vec3(PacMan.y *2.0f,0.0f , PacMan.x*2.0f);
	target = eye;
	switch (yi)
	{
	case UP:
		target.z -= 5.0f;
		break;
	case DOWN:
		eye = glm::vec3(PacMan.y *2.0f, 0.0f, PacMan.x*2.0f);
		target.z += 5.0f;
		break;
	case LEFT:
		eye = glm::vec3(PacMan.y *2.0f, 0.0f, PacMan.x*2.0f);
		target.x -= 5.0f;
		break;
	case RIGHT:
		eye = glm::vec3(PacMan.y *2.0f, 0.0f, PacMan.x*2.0f);
		target.x += 5.0f;
		break;
	}
	
	

	for (int i = 0; i < map.size(); ++i)
		for (int j = 0; j < map[i].size(); ++j)
		{
			transform = glm::translate(glm::mat4(1.0f), glm::vec3(i * 2, 0.0f, j * 2));
			transform = glm::perspective(45.0f, 1024.0f / 768.0f, 0.7f, 100.0f) *
				camera * transform;
			triangle.draw(glm::translate(glm::scale(transform,glm::vec3(1.0f,.1f,1.0f)),glm::vec3(0.0f,-6.0f,0.0f)), glm::vec4(0.5f, 0.0f, 1.0f, 1.0f));
			if (map[i].at(j) == wall)
			{
				triangle.draw(transform, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
			}
			else if (map[i].at(j) == coins)
			{
				triangle.draw(glm::scale(transform, glm::vec3(0.2f, 0.2f, 0.2f)), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
			}
			else if (map[i].at(j) == pacman)
			{
				triangle.draw(glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
			}
			else if (map[i].at(j) == ghost)
			{
				triangle.draw(glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f)), ghostColor[rand() % 4]);
			}
		}
}


void Application::displayEditor()
{

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		eye = glm::vec3(10.0f, 95.0f, 5.0f);
		target = glm::vec3(18.0f, 0.0f, 18.0f);

	int _x, _y, num;
	if (write)
	{
		write = false;

		std::cin >> _x >> _y >> num;
		ChangeNumber(_x, _y, num);
	}

	



	for (int i = 0; i < map.size(); ++i)
		for (int j = 0; j < map[i].size(); ++j)
		{
			transform = glm::translate(glm::mat4(1.0f), glm::vec3(i * 2, 0.0f, j * 2));
			transform = glm::perspective(100.0f, 1024.0f / 768.0f, .1f, 100.0f) *
				camera * transform;
			triangle.draw(glm::scale(transform,glm::vec3(1.0f,.1f,1.0f)), glm::vec4(0.5f, 0.0f, 1.0f, 1.0f));
			if (map[i].at(j) == wall)
			{
				triangle.draw(transform, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
			}
			else if (map[i].at(j) == coins)
			{
				triangle.draw(glm::scale(transform, glm::vec3(0.3f, 0.3f, 0.3f)), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
			}
			else if (map[i].at(j) == pacman)
			{
				triangle.draw(glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
			}
			else if (map[i].at(j) == seen);
			else if (map[i].at(j) == ghost)
			{
				triangle.draw(glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f)), ghostColor[rand() % 4]);
			}

			
		}


}

void Application::keyboard(int key, int scancode, int actions, int mods)
{
	if (actions == GLFW_RELEASE)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE);

			break;

		case GLFW_KEY_P:
			Edit = !Edit;
			break;

		case GLFW_KEY_W:
			if (Edit)
			{
				goUp();
				yi = UP;
			}
			break;

		case GLFW_KEY_S:
			if (Edit)
			{
				goDown();
				yi = DOWN;
			}
			break;
		case GLFW_KEY_D:
			if (Edit)
			{
				goRight();
				yi = RIGHT;
			}
			break;
		case GLFW_KEY_A:
			if (Edit)
			{
				goLeft();
				yi = LEFT;
			}
			break;
		case GLFW_KEY_T:
			write = true;
			break;
		}
	}

}


void Application::setup()
{
	triangle.Shaders("vertex.vs","Fragment.fs");
	triangle.setup(vertexPositions, NULL);
	Edit = true;
	write = false;

	uno.setup(1,1);
	dos.setup(1, 18);
	tres.setup(18, 1);
	cuatro.setup(18, 18);


	PacMan.setup(map);
}




void Application::display()
{
	
	if (Edit)
	{
		displayGame();
	}
	else
	{
		displayEditor();
	}


	
	
}

void Application::reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void Application::goUp()
{
	int posY = PacMan.x - 1;
	int posX = PacMan.y;
	if (map[posX].at(posY) == 1)
	{}
	else if (map[posX].at(posY) == 0 || map[posX].at(posY) == 3)
	{
		map[PacMan.y].at(PacMan.x) = 3;
		map[posX].at(posY) = 2;
		PacMan.x = posY;
		PacMan.y = posX;
	}
}

void Application::goDown()
{
	int posY = PacMan.x + 1;
	int posX = PacMan.y;
	if (map[posX].at(posY) == 1)
	{
	}
	else if (map[posX].at(posY) == 0 || map[posX].at(posY) == 3)
	{
		map[PacMan.y].at(PacMan.x) = 3;
		map[posX].at(posY) = 2;
		PacMan.x = posY;
		PacMan.y = posX;
	}
}

void Application::goLeft()
{
	int posY = PacMan.x;
	int posX = PacMan.y - 1;
	if (map[posX].at(posY) == 1)
	{
	}
	else if (map[posX].at(posY) == 0 || map[posX].at(posY) == 3)
	{
		map[PacMan.y].at(PacMan.x) = 3;
		map[posX].at(posY) = 2;
		PacMan.x = posY;
		PacMan.y = posX;
	}
}

void Application::goRight()
{
	int posY = PacMan.x;
	int posX = PacMan.y + 1;
	if (map[posX].at(posY) == 1)
	{
	}
	else if (map[posX].at(posY) == 0 || map[posX].at(posY) == 3)
	{
		map[PacMan.y].at(PacMan.x) = 3;
		map[posX].at(posY) = 2;
		PacMan.x = posY;
		PacMan.y = posX;
	}
}

void Application::ChangeNumber(int _x, int _y, int newNum)
{
	if (newNum < 0 && newNum > 3)
	{
		std::cout << "ingresa un numero valido";
	}
	else
		map[_x].at(_y) =  newNum;
}
