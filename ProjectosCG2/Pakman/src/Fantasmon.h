#pragma once
#include <iostream>
#include "Application.h"
class Fantasmon
{
public:
	Fantasmon();
	~Fantasmon();

	int x, y;

	void moveUp(std::vector<std::vector<int> >);
	void moveDown(std::vector<std::vector<int> >);
	void moveLeft(std::vector<std::vector<int> >);
	void moveRight(std::vector<std::vector<int> >);

	bool CheckPacman(std::vector<std::vector<int> >);

};

