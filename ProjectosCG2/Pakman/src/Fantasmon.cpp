#include "Fantasmon.h"



Fantasmon::Fantasmon()
{
}


Fantasmon::~Fantasmon()
{
}

void Fantasmon::setup(int _x, int _y)
{
	x = _x;
	y = _y;
}

bool Fantasmon::moveUp(std::vector<std::vector<int> >& map)
{

	if (map[x - 1].at(y) == 1)
		return false;
	else if (map[x - 1].at(y) == 0)
	{
		map[x].at(y) == 0;
		map[x - 1].at(y) == 4;
		x = x - 1;
		return true;
	}
	else if (map[x - 1].at(y) == 3)
	{
		map[x].at(y) == 3;
		map[x - 1].at(y) == 4;
		x = x - 1;
		return true;
	}

	return false;

}

bool Fantasmon::moveDown(std::vector<std::vector<int> >& map)
{
	if (map[x - 1].at(y) == 1)
		return false;
	else if (map[x + 1].at(y) == 0)
	{
		map[x].at(y) == 0;
		map[x + 1].at(y) == 4;
		x = x + 1;
		return true;
	}
	else if (map[x + 1].at(y) == 3)
	{
		map[x].at(y) == 3;
		map[x + 1].at(y) == 4;
		x = x + 1;
		return true;
	}
	return false;
}

bool Fantasmon::moveLeft(std::vector<std::vector<int> >& map)
{
	if (map[x].at(y -1) == 1)
		return false;
	else if (map[x].at(y - 1) == 0)
	{
		map[x].at(y) == 0;
		map[x].at(y-1) == 4;
		y = y - 1;
		return true;
	}
	else if (map[x].at(y -1) == 3)
	{
		map[x].at(y) == 3;
		map[x].at(y - 1) == 4;
		y = y - 1;
		return true;
	}
	return false;
}

bool Fantasmon::moveRight(std::vector<std::vector<int> >& map)
{
	if (map[x].at(y + 1) == 1)
		return false;
	else if (map[x].at(y + 1) == 0)
	{
		map[x].at(y) == 0;
		map[x].at(y + 1) == 4;
		y = y + 1;
		return true;
	}
	else if (map[x].at(y + 1) == 3)
	{
		map[x].at(y) == 3;
		map[x].at(y + 1) == 4;
		y = y + 1;
		return true;
	}
	return false;
}

bool Fantasmon::CheckPacman(std::vector<std::vector<int> > map)
{
	if (map[x - 1].at(y) == 2)
		return true;
	else if (map[x + 1].at(y) == 2)
		return true;
	else if (map[x].at(y -1) == 2)
		return true;
	else if (map[x].at(y+1) == 2)
		return true;
	else
		return false; 
}

void Fantasmon::artificialIneligence(std::vector<std::vector<int>>& map, Player gamer)
{
	if (x != gamer.x)
	{

		if (x > gamer.x)
		{
			moveDown(map);
		}
		else
			moveUp(map);
	}
	else
	{
		if (y != gamer.y)
		{
			if (y > gamer.y)
			{
				moveLeft(map);
			}
			else
				moveRight(map);
		}
	}

}
